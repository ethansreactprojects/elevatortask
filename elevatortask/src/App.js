import logo from './logo.svg';
import './App.css';
import Slider, { SliderThumb } from '@mui/material/Slider';
import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';
import ElevatorIcon from '@mui/icons-material/Elevator';
import PropTypes from 'prop-types';
import { useState, useEffect, useImperativeHandle, forwardRef, useRef, useMemo } from 'react';
import Queue from "../src/Queue";
import { Stack } from '@mui/material';
import { Paper } from '@mui/material';
import { Typography } from '@mui/material';
import { withStyles } from '@mui/styles';
import { Button } from '@mui/material';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const ElevatorVerticalSlider = forwardRef((props, ref) => {
  function ElevatorThumbComponent(props) {
    const { children, ...other } = props;
    return (
      <SliderThumb {...other}>
        <ElevatorIcon fontSize='large' />
      </SliderThumb>
    );
  }
  const busyColor = "#eb4034";
  const arrivedColor = "#43eb34";
  const [queue, setQueue] = useState(new Queue());
  const [color, setColor] = useState(props.color);
  const levelMultiply = 6;
  let intervalId = 0;
  const [requiredLevel, setRequiredLevel] = useState(0);
  const [currentLevelProgression, setCurrentLevelProgression] = useState(0);
  const [moving, setMoving] = useState(false);
  const [busy, setBusy] = useState(false);
  const [currentLevel, setCurrentLevel] =
    useState(currentLevelProgression * levelMultiply);

  const stateRef = useRef();
  stateRef.current = busy;
  const queueRef = useRef();
  queueRef.current = queue;
  const currLevelRef = useRef();
  currLevelRef.current = currentLevel;

  useImperativeHandle(ref, () => ({
    moveTo: (level) => {
      console.log(level);
      if (level !== queueRef.current.peekEnd() && level >= 0 && level <= props.NumberOfLevels && level !== currLevelRef.current) {
        queueRef.current.enqueue(level);
        checkQueue();
      }
    },
    isBusy: () => {
      //console.log(moving, busy, stateRef, );
      console.log(stateRef.current, queueRef.current.isEmpty());
      return stateRef.current || !queueRef.current.isEmpty();
    },
    alreadyAvailableFor: (level) => {
      return !stateRef.current && level === currLevelRef.current;
    },
  }), [moving, busy, queue, stateRef, currentLevel]);



  function preventHorizontalKeyboardNavigation(event) {
    if (event.key === 'ArrowLeft' || event.key === 'ArrowRight') {
      event.preventDefault();
    }
  }
  const marks = [];
  for (let i = 0; i < props.NumberOfLevels; i++) {
    marks.push({ value: i * levelMultiply, label: 'level: ' + i });
  }
  const marksHidden = [];
  for (let i = 0; i < props.NumberOfLevels; i++) {
    marksHidden.push({ value: i * levelMultiply, label: '' });
  }
  function movement() {
    console.log("moving to", requiredLevel, "currently at ", currentLevelProgression, currentLevel, busy);
    if (requiredLevel !== currentLevel) {
      setBusy(true);
      if (requiredLevel > currentLevel
        && requiredLevel * levelMultiply > currentLevelProgression) {
        setCurrentLevelProgression(currentLevelProgression + 1);
        console.log("incresed");
      }
      else if (requiredLevel < currentLevel &&
        requiredLevel * levelMultiply < currentLevelProgression) {
        setCurrentLevelProgression(currentLevelProgression - 1);
        console.log("decreased");
      }
      else if (moving) {
        console.log("reached");
        setColor(arrivedColor);
        setCurrentLevel(requiredLevel);
        setMoving(false);
        setBusy(false);
        setTimeout(() => {
          setColor(props.color);
        }, 1500)
      }
    }
  }

  useEffect(() => {
    //runs when required level or current level progression changes
    // we should perform movement
    setTimeout(() => {
      movement();
    }, 100)
  }, [requiredLevel, currentLevelProgression]);

  useEffect(() => {
    if (moving)// moving changed to true, we should set the required level.
    {
      setTimeout(() => {
        console.log("abou to start new call - waiting 2 secs prior to it.");
        setColor(busyColor);
        let required = queue.dequeue();
        console.log("req", required);
        setRequiredLevel(required);
      }, 2000)
    }
    else {

    }
  }, [moving]);

  function checkQueue() {
    if (intervalId === 0) {
      intervalId = setInterval(() => {
        //console.log("q", queue, queue.isEmpty(), queue.peek(), "moving", moving);
        if (!queue.isEmpty() && !moving) {
          setMoving(true);
        }
        else if (queue.isEmpty()) {
          clearInterval(intervalId);
          intervalId = 0;
        }
      }, 100);
    }
  }

  return (
    <Box sx={{ height: '90vh' }}>
      <Slider
        sx={{
          '& input[type="range"]': {
            WebkitAppearance: 'slider-vertical',
          },
          '& .Mui-disabled': {
            color: color
          },
          '& .MuiSlider-thumb': {
            height: 25,
            width: 25,
            backgroundColor: '#fff',
            border: '1px solid currentColor',
            '&:hover': {
              boxShadow: '0 0 0 8px rgba(58, 133, 137, 0.16)',
            },
          },
          '& .MuiSlider-track': {
            height: 3,
            color: color
          },
          '& .MuiSlider-rail': {
            color: color,
            opacity: 1,
            height: 3,
          },

        }}
        orientation="vertical"
        components={{
          Thumb: ElevatorThumbComponent,
        }}
        aria-label="Temperature"
        onKeyDown={preventHorizontalKeyboardNavigation}
        disabled
        marks={props.showMarks ? marks : marksHidden}
        value={currentLevelProgression}
        min={0}

        size="small"
        max={props.NumberOfLevels * levelMultiply}
      />
    </Box>
  );
});
ElevatorVerticalSlider.propTypes = {
  NumberOfLevels: PropTypes.number,
  color: PropTypes.string,
  showMarks: PropTypes.bool,
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function App() {
  let levelNums = 10;
  let elevatorNums = 5;
  let extraDataRows = 2;
  let rows = [];
  const itemsEls = useRef(new Array())
  const buttonsRefs = useRef(new Array())
  //const [callQueue, setCallQueue] = useState(new Queue());

  useEffect(() => {
    /*
    itemsEls.current[0].moveTo(1);
    itemsEls.current[1].moveTo(2);
    itemsEls.current[2].moveTo(0);
    itemsEls.current[3].moveTo(9);*/
    //console.log(itemsEls);
  }, [itemsEls]);

  for (let i = 0; i < elevatorNums + extraDataRows; i++) {
    if (i === 0) {
      let columns = [];
      const getRef = (element) => {
        buttonsRefs.current.push(element)
      };
      for (let j = 0; j < levelNums; j++) {
        columns.push((<Button variant='outlined' key={j} color='primary' ref={getRef} style={{}}
          onClick={() => {
            console.log(j);
            //callQueue.enqueue(j);
            console.log(buttonsRefs.current[j].innerText);
            let freeIndex = -1;
            let alreadyAvailable = false;
            for (let z = 0; z < elevatorNums; z++) {
              if (itemsEls.current[z].alreadyAvailableFor(j)) {
                console.log(z + " already at " + j);
                alreadyAvailable = true;
                break;
              }
            }
            if (!alreadyAvailable) {
              for (let z = 0; z < elevatorNums; z++) {
                if (itemsEls.current[z].alreadyAvailableFor(j)) {
                  console.log(z + " already at " + j);
                  alreadyAvailable = true;
                  break;
                }
                if (itemsEls.current[z].isBusy()) {
                  console.log(z + "is Busy");
                }
                else {
                  console.log(z + "is free");
                  freeIndex = z;
                  break;
                }
              }
              if (freeIndex === -1) {
                freeIndex = getRandomInt(elevatorNums);
              }
              itemsEls.current[freeIndex].moveTo(j);

              let prevTxt = buttonsRefs.current[j].innerText;
              let timer = 0;
              let intervalId = setInterval(() => {
                buttonsRefs.current[j].innerText = "waiting: " + timer + "sec.";
                timer++;
                if (itemsEls.current[freeIndex].alreadyAvailableFor(j)) {
                  clearInterval(intervalId);
                  buttonsRefs.current[j].innerText = "Arrived";
                  document.getElementById("audio").play();
                  setTimeout(() => {
                    buttonsRefs.current[j].innerText = prevTxt;
                  }, 1500)
                }
              }, 1000);
            }
            else {
              let prevTxt = buttonsRefs.current[j].innerText;
              buttonsRefs.current[j].innerText = "Arrived";
              document.getElementById("audio").play();
              setTimeout(() => {
                buttonsRefs.current[j].innerText = prevTxt;
              }, 1500)
            }
          }} >
          Call to level: {j}
        </Button>));
      }
      rows.push(
        (<Stack direction="column-reverse" key={i} spacing={"5vh"}
          style={{ height: '100vh' }}>
          {columns}
        </Stack>)
      );
    }
    else if (i === elevatorNums + extraDataRows - 1) {//last index

    }
    else {
      console.log("index:", i);
      const getRef = (element) => {
        itemsEls.current.push(element)
      };
      let showMarks = i + 1 === elevatorNums + extraDataRows - 1;
      rows.push((<Stack direction="column-reverse" key={i} paddingBottom={"1%"}>
        <ElevatorVerticalSlider NumberOfLevels={levelNums} ref={getRef} color="#4287f5" showMarks={showMarks} />
      </Stack>)
      );
    }
  }

  return (
    <div className="App" style={{ backgroundColor: "GrayText", height: "100vh" }}>
      <Stack direction="row" spacing={2} paddingY={2} height='100%' style={{ backgroundColor: "white" }}>
        {rows}
      </Stack>
      <audio id={"audio"} src={process.env.PUBLIC_URL+'/assets/ding.mp3'}/>
    </div>
  );
}

export default App;
